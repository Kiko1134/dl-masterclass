import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ArrayListTest {

    ArrayList<Integer> arrayList = new ArrayList<>(10);
    ArrayList<Integer> arrayList1 = new ArrayList<>(10);

    @BeforeEach
    void setUp() {
        arrayList.add(1);
        arrayList.add(2);
        arrayList.add(3);

        arrayList1.add(4);
        arrayList1.add(5);
        arrayList1.add(6);
    }

    @Test
    @DisplayName("Get element from array")
    void get() {

        assertEquals(1,arrayList.get(0));
    }

    @Test
    void set() {
        arrayList.set(1,189);
        assertEquals(189,arrayList.get(1));
    }


    @Test
    void size() {
        assertEquals(3,arrayList.size());
    }

    @Test
    void capacity() {
        assertEquals(10,arrayList.capacity());
    }

    @Test
    void isEmpty() {
        assertFalse(arrayList.isEmpty());
    }

    @Test
    void clear() {
        arrayList.clear();
        assertEquals(0,arrayList.size());
    }

    @Test
    void remove() {
        arrayList.remove();
        assertEquals(2,arrayList.size());
    }

    @Test
    void concat() {
        arrayList.concat(arrayList1);
        assertEquals(5, arrayList.get(4));
    }

    @Test
    void resize() {
        arrayList.resize(20);
        assertEquals(20,arrayList.capacity());
    }

    @Test
    void shrinkToFit() {
        arrayList1.shrinkToFit();
        assertEquals(3,arrayList1.capacity());
    }
}