import java.util.Arrays;

/**
 * @param <T> Using generics makes this class compatible for multiple types
 */
public class ArrayList <T>{
    private T[] arr;
    private int i = 0;

    public ArrayList(int capacity) {
        arr = (T[]) new Object[capacity];
    }

    public ArrayList() {arr = (T[]) new Object[10];}

    /**
     * This function returns the object at the specified index
     * @param i index of object in the array
     * @return the object a the specified index
     */
    public Object get(int i){
        if(arr[i] != null)
            return arr[i];
        else
            throw new ArrayIndexOutOfBoundsException(i);
    }


    /**
     * This function sets a value at index
     * @param index the index in the array
     * @param t the element we want to set at the specified index
     */
    public void set(int index, T t){
        if(arr[index] == null)
            throw new ArrayIndexOutOfBoundsException(i);
        arr[index] = t;
    }

    /**
     * This function add an element at the end of the array
     * @param t the element we want to add to the array
     */
    public void add(T t){
        if(i == arr.length){
            arr = Arrays.copyOf(arr,arr.length * 2);
        }
        arr[i] = t;
        ++i;
    }

    /**
     * This function returns the size of the array
     * @return the size of the array
     */
    public int size(){
        int size = 0;
        for(Object o: arr){
            if(o != null)
                size++;
        }
        return size;
    }


    /**
     * This function returns the capacity of the buffer space
     * @return the capacity of the buffer space
     */
    public int capacity(){
        int size = 0;
        for(Object o: arr){
                size++;
        }
        return size;
    }

    /**
     * This function shows us if the array is empty or not
     * @return if the array is empty or not
     */
    public boolean isEmpty(){
        return arr.length == 0;
    }

    /**
     * This function cleans every element in the array
     */
    public void clear(){
        for(int j = i; j> 0; j--){
            --i;
        }
        arr = (T[]) new Object[0];
    }

    /**
     * This function removes element from the tail of the array
     */
    public void remove(){
        arr[i-1] = null;
        i--;
    }

    /**
     * @param concat_arr the array we want to concatenate
     * This function concatenates two arrays
     */
    public void concat(ArrayList<T> concat_arr){
        for(int k = 0; k<concat_arr.size();k++){
            arr[i] = (T)concat_arr.get(k);
            i++;
        }
    }

    /**
     * This function is used to resize the array at a specified size
     * @param size the wanted size for resizing the array
     */
    public void resize(int size){
        if(size < 0)
            System.out.println("Cannot resize under 0");
        else if(size == 0){
            for(int j = i; j> 0; j--){
                --i;
            }
            arr = Arrays.copyOf(arr,0);
        }
        if(arr.length == size)
            System.out.println("The size of the array is unchanged");
        else
            arr = Arrays.copyOf(arr,size);
    }

    /**
     * This function resizes the buffer to be as efficient as possible
     */
    public void shrinkToFit(){
        arr = Arrays.copyOf(arr,i);
    }

    /**
     * This function is used to swap the elements from one array to another
     * @param swap_arr The array we want to swap values with
     */
    public void swap(ArrayList<T> swap_arr){

        T[] new_arr = (T[]) new Object[swap_arr.size()];

        for(int x = 0; x<swap_arr.size();x++){
            T t = (T)swap_arr.get(x);
            new_arr[x] = t;
        }
        for(int p = i-1; p>=0;p--){
            T o = new_arr[p];
            new_arr[p] = arr[p];
            arr[p] = o;
        }

        for(int x = 0; x<swap_arr.size();x++){
            T t = new_arr[x];
            swap_arr.set(x,t);
        }

    }

}